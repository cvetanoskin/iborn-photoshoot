package services.Impl;

import api.model.UserDTO;
import domain.User;
import services.UserDetailsService;

import java.util.List;
import java.util.Optional;

public class UserDetailsServiceImpl implements UserDetailsService {

    @Override
    public List<Long> getAllowedUsers(String username) {
        return null;
    }

    @Override
    public User createUser(UserDTO userDTO) {
        return null;
    }

    @Override
    public boolean sendUserCreateNotification(User user) {
        return false;
    }

    @Override
    public boolean validateUser(UserDTO userDTO, boolean activateWithPassword) {
        return false;
    }

    @Override
    public boolean sendRegisterDoctorEmail(String appUrl, String entityName) {
        return false;
    }

    @Override
    public boolean sendRegisterNurseEmail(String appUrl, String entityName) {
        return false;
    }

    @Override
    public boolean sendRegisterUserEmail(String appUrl, String entityName) {
        return false;
    }

    @Override
    public boolean sendChangeUsernameEmail (User user) {
        return false;
    }

    @Override
    public User update (Optional<User> user) {
        return null;
    }
}

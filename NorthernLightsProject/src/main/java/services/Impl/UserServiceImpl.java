package services.Impl;

import api.mapper.UserMapper;
import api.model.UserDTO;
import domain.User;
import lombok.extern.slf4j.Slf4j;
import repositories.UserRepository;
import services.UserDetailsService;
import services.UserService;

import java.util.List;
import java.util.Optional;

@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final UserDetailsService userDetailsService;

    public UserServiceImpl(UserRepository userRepository, UserMapper userMapper, UserDetailsService userDetailsService) {
        this.userRepository = userRepository;
        this.userMapper = userMapper;
        this.userDetailsService = userDetailsService;
    }

    @Override
    public UserDTO updateUser(String username, Long id) {
        List<Long> allowedUsersIds = userDetailsService.getAllowedUsers(username);
        UserDTO savedUserDTO = null;
        if (!(allowedUsersIds.contains(id))) {
            throw new IllegalArgumentException("Accessing invalid users");
        }

        log.debug("Updating user with id {}", id);

        Optional<User> user = userRepository.findById(id);

        if (user.isPresent()) {
            savedUserDTO = userMapper.userToUserDTO(userDetailsService.update(user));
        }

        log.debug("Updated user with id {}", id);

        return savedUserDTO;
    }

    @Override
    public void createNewUser(Long userId, UserDTO userDTO) {

        log.debug("Creating user with id {}", userId);

        User loggedInUser = userRepository.findById(userId).get();
        User user = userDetailsService.createUser(userDTO);
        switch (loggedInUser.getRole()) {
            case "Administrator":
                user.setRole("Administrator");
                break;
            case "Doctor":
                user.setRole("Doctor");
                break;
            case "Nurse":
                user.setRole("Nurse");
                break;
            case "Patient":
                user.setRole("Patient");
                break;
            case "Maid":
                user.setRole("Maid");
                break;
            case "Trainee":
                user.setRole("Trainee");
                break;
        }

        userRepository.save(user);

        try {
            if (!user.getUsername().equals("") && user.getUsername() != null) {
                log.debug("Sending notification to {}", user.getUsername());
                userDetailsService.sendUserCreateNotification(user);
            }
        } catch (Exception e) {
            log.error("Error sending notification to {}, with message: {}", user.getUsername(), e.getMessage());
        }
    }

    @Override
    public void registerNewUserAccount(UserDTO userDTO, String appUrl, boolean activateWithPassword, String entityName, boolean recaptchaEnabled) {

        if (!recaptchaEnabled) {
            throw new IllegalArgumentException("Invalid captcha");
        } else {
            userDetailsService.validateUser(userDTO, !activateWithPassword);
        }
        if (userDTO.getRole() != null) {
            switch (userDTO.getRole()) {

                case "Doctor":
                    userDTO.setRole("Doctor");
                    break;
                case "Nurse":
                    userDTO.setRole("Nurse");
                    break;
            }
            userRepository.save(userMapper.userDTOToUser(userDTO));
        }

        User user = userMapper.userDTOToUser(userDTO);

        try {

            if (activateWithPassword) {
                if (user.getRole().equals("Doctor")) {
                    userDetailsService.sendRegisterDoctorEmail(appUrl, entityName);
                } else if (user.getRole().equals("Nurse")) {
                    userDetailsService.sendRegisterNurseEmail(appUrl, entityName);
                }
            } else {
                userDetailsService.sendRegisterUserEmail(appUrl, entityName);
            }
            userRepository.save(user);
        } catch (Exception e) {
            log.error("Error sending notification to {}, with message: {}", user.getUsername(), e.getMessage());
        }
    }

    @Override
    public void delete(String username, long id) {
        List<Long> allowedUsersIds = userDetailsService.getAllowedUsers(username);
        if (!allowedUsersIds.contains(id)) {
            throw new IllegalArgumentException("Accessing invalid users");
        }

        log.debug("Deleting user with id {}", id);
        User user = userRepository
                .findById(id).get();

        userRepository.delete(user);

        log.debug("Deleted user with id {} ", id);
    }

    @Override
    public String changeUserUserName(Long userId, String newUserName) {

        User user = userRepository.findById(userId).get();
        if (user.getUsername().equals(newUserName)) {
            return "New username cannot be the same as the old one";
        }

        user.setUsername(newUserName);

        userRepository.save(user);

        userDetailsService.sendChangeUsernameEmail(user);

        userRepository.save(user);

        return null;
    }

}

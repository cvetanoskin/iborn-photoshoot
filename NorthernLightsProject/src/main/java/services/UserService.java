package services;

import api.model.UserDTO;

public interface UserService {

    UserDTO updateUser(String username, Long id);
    void createNewUser(Long userId, UserDTO userDTO);
    void registerNewUserAccount(UserDTO userDTO, String appUrl, boolean activateWithPassword, String entityName, boolean recaptchaEnabled);
    String changeUserUserName(Long userId, String newUserName);
    void delete(String username, long id);
}

package services;

import api.model.UserDTO;
import domain.User;

import java.util.List;
import java.util.Optional;

public interface UserDetailsService {
    List<Long> getAllowedUsers(String username);
    User createUser(UserDTO userDTO);
    boolean sendUserCreateNotification(User user);
    boolean validateUser(UserDTO userDTO, boolean activateWithPassword);
    boolean sendRegisterDoctorEmail(String appUrl, String entityName);
    boolean sendRegisterNurseEmail(String appUrl, String entityName);
    boolean sendRegisterUserEmail(String appUrl, String entityName);
    boolean sendChangeUsernameEmail (User user);
    User update (Optional<User> user);
}

package domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class User {
    String username;
    String role;
}

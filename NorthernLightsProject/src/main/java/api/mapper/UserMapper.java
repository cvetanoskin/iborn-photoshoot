package api.mapper;


import api.model.UserDTO;
import domain.User;

public interface UserMapper {
    UserDTO userToUserDTO(User user);
    User userDTOToUser(UserDTO userDTO);
}
